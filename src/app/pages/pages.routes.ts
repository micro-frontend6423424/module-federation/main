import { Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { TestCompComponent } from './test-comp/test-comp.component';


export const routes: Routes = [
    {
        path: "",
        component: PagesComponent,
        children: [
            {
                path: "test-cmp",
                component: TestCompComponent
            },
            {
                path: "app-one",
                loadChildren: () => import("appOne/routes").then(m => m.routes).catch(e => console.error(e))
            },
            // {
            //     path: "react-remote",
            //     loadChildren: () => import("reactRemote/routes").then(m => m.routes).catch(e => console.error(e))
            // },
            {
                path: "",
                redirectTo: "app-one",
                pathMatch: "full"
            }
        ]
    },
];
